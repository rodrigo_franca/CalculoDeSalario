/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.calculodesalario;

/**
 As regras de negocio são as seguintes:
 * 
 Desenvolvedor possuem 20% de desconto caso seu salario seja maior do que R$ 3.000,00. Caso contrario o desconto e de 10%.
 * 
 * BDAs e testadores possuem desconto de 25% se seus salários forem maiores do que R$ 2.500,00 15% em caso contrario.

 */
public class CalculadoraDeSalario {


    
    public double CalcularSalario (Funcionario funcionario){
        
        if (funcionario.getCargo() == Cargo.DESENVOLVEDOR && funcionario.getSalario()<3000){
            
        return funcionario.getSalario() - (funcionario.getSalario()*0.10);
        
        } 
        
        return 0;
    
       
    }
    
     public double CalcularSalarioDba (Funcionario funcionario){
        
        if (funcionario.getCargo() == Cargo.DBA && funcionario.getSalario()<3000){
            
        return funcionario.getSalario() - (funcionario.getSalario()*0.10);
        
        } 
        
        return 0;
    
}
}
    
    
    
    

