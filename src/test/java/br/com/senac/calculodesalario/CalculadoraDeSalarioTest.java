/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.calculodesalario;

import org.junit.Assert;
import org.junit.Test;

/**
 As regras de negocio são as seguintes:
 * 
 Desenvolvedor possuem 20% de desconto caso seu salario seja maior do que R$ 3.000,00. Caso contrario o desconto e de 10%.
 * 
 * BDAs e testadores possuem desconto de 25% se seus salários forem maiores do que R$ 2.500,00 15% em caso contrario.

 */

public class CalculadoraDeSalarioTest {
    
@Test
    public void SalarioDesemvolvedorAbaixoDoLimite (){
        
        CalculadoraDeSalario calculador = new CalculadoraDeSalario();
        Funcionario funcionario = new Funcionario("Rodrigo", 3000, Cargo.DESENVOLVEDOR);
        double salario = calculador.CalcularSalario(funcionario);
        
        
        Assert.assertEquals(900, salario, 0);
        
    
    }
    
    @Test
    public void SalarioDbaAbaixoDoLimite (){
        
        CalculadoraDeSalario calculador = new CalculadoraDeSalario();
        Funcionario funcionario = new Funcionario("Rodrigo", 5000, Cargo.DBA);
        double salario = calculador.CalcularSalario(funcionario);
        
        
        Assert.assertEquals(900, salario, 0);
        
    
    }
    
    
    
}
